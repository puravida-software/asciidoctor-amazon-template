<<<

ifeval::["{backend}" != "html5"]
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
{empty} +
endif::[]

[NOTE]
====
Usted es libre de:

*Compartir* — copiar y redistribuir el material en cualquier medio o formato

*Adaptar* — remezclar, transformar y crear a partir del material
para cualquier finalidad, incluso comercial.

_El licenciador no puede revocar estas libertades mientras cumpla con los términos de la licencia._

image::by-nc-sa.png[]
====

<<<
